<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label')->nullable();
            $table->unsignedBigInteger('question_id')->nullable();
            $table->timestamps();
			$table->foreign('question_id')->references('id')
					->on('questions')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('answers', function($table){
            $table->dropForeign('answers_question_id_foreign');
        });
        Schema::dropIfExists('answers');
    }
}
