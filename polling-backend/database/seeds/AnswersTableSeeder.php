<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Answer::insert([
            ['label' => 'Google Pixel (e.g. 3, 3 XL)', 'question_id' => \App\Question::first()->id],
            ['label' => 'Apple iPhone (e.g. 8, X, XS)', 'question_id' => \App\Question::first()->id],
            ['label' => 'Samsung Galaxy (e.g. S9, S9+)', 'question_id' => \App\Question::first()->id],
            ['label' => 'Huawei (e.g. P20 Pro, 20 Mate)', 'question_id' => \App\Question::first()->id],
            ['label' => 'None of the above', 'question_id' => \App\Question::first()->id],
        ]);
    }
}
