<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Question::insert([
            'title' => 'Which of these phones have you heard of? (Select all that apply)'
        ]);
    }
}
