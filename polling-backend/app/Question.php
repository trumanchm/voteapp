<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function getResponses()
    {
        $results = array();
        foreach($this->answers()->get() as $answer) {
            $results[$answer->id] = $answer->responses()->count();
        }
        return $results;
    }
}
