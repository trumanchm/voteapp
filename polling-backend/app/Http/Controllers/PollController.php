<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Response;

class PollController extends Controller
{
    public function submitVote(Request $request) {
        $votes = $request->data;
        foreach($votes as $vote) {
            $response = new Response;
            $response->saveResponse($vote);
        }
        return response()->json(['results' => "success"]);
    }

    public function getResults(Request $request) {
        $question = Question::find(1);
        $results = $question->getResponses();

        return response()->json(['results' => $results]);
    }
}
