<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }

    public function saveResponse($answerId) {
        $this->answer()->associate($answerId);
        $this->save();
    }
}
