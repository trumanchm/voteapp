import axios from 'axios';

export default axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}/api`,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
})