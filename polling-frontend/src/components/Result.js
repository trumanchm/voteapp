import React from 'react';
import _ from 'lodash';
import Answer from './Answer';
import Terms from './Terms';
import polling from '../apis/polling';
import Question from './Question';

class Result extends React.Component {
  state = { results: [] }

  async componentDidMount() {
    try {
      const response = await polling.get('/results');
      const results = response.data.results;
      const sum = _.sum(Object.values(results));  // convert object to array and calculate total response

      this.setState({ results, sum })
    } catch (err) {
      console.error(err);
    }
  }

  render() {
    const { selectedAnswers } = this.props.location.state;
    const { sum, results } = this.state;

    return (
      <div className="question-wrapper">

        <Question label="Which of these phones have you heard of? (Select all that apply)" />

        <Answer
          content="Google Pixel (e.g. 3, 3 XL)"
          selected={selectedAnswers.includes(1)}
          answerId={1}
          disabled={true}
          response={results['1']}
          sum={sum}
        />
        <Answer
          content="Apple iPhone (e.g. 8, X, XS)"
          selected={selectedAnswers.includes(2)}
          answerId={2}
          disabled={true}
          response={results['2']}
          sum={sum}
        />
        <Answer
          content="Samsung Galaxy (e.g. S9, S9+)"
          selected={selectedAnswers.includes(3)}
          answerId={3}
          disabled={true}
          response={results['3']}
          sum={sum}
        />
        <Answer
          content="Huawei (e.g. P20 Pro, 20 Mate)"
          selected={selectedAnswers.includes(4)}
          answerId={4}
          disabled={true}
          response={results['4']}
          sum={sum}
        />
        <Answer
          content="None of the above"
          selected={selectedAnswers.includes(5)}
          answerId={5}
          disabled={true}
          response={results['5']}
          sum={sum}
        />
        <Terms />
      </div>
    )
  }
}

export default Result;