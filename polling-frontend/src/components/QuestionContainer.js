import React from 'react';
import _ from 'lodash';
import Answer from './Answer';
import Terms from './Terms';
import history from '../history';
import polling from '../apis/polling';
import Question from './Question';

class QuestionContainer extends React.Component {
  state = { selectedAnswers: [], submitted: false };

  onClickAnswer = id => {
    this.setState(state => ({ selectedAnswers: _.xor(state.selectedAnswers, [id]) }))
  }

  onSubmitVote = async () => {
    if (!this.state.selectedAnswers.length) {
      alert("Please select at least one option.");
      return;
    }
    this.setState({ submitted: true });
    try {
      await polling.post('/submit', {
        data: this.state.selectedAnswers
      });
      history.push('/results', { selectedAnswers: this.state.selectedAnswers });

    } catch (err) {
      console.error(err);
    }
  }

  render() {
    const { selectedAnswers, submitted } = this.state;
    return (
      <div className="question-wrapper">
        <Question label="Which of these phones have you heard of? (Select all that apply)" />

        <Answer
          content="Google Pixel (e.g. 3, 3 XL)"
          selected={selectedAnswers.includes(1)}
          onClickAnswer={this.onClickAnswer}
          answerId={1}
          disabled={submitted}
        />
        <Answer
          content="Apple iPhone (e.g. 8, X, XS)"
          selected={selectedAnswers.includes(2)}
          onClickAnswer={this.onClickAnswer}
          answerId={2}
          disabled={submitted}
        />
        <Answer
          content="Samsung Galaxy (e.g. S9, S9+)"
          selected={selectedAnswers.includes(3)}
          onClickAnswer={this.onClickAnswer}
          answerId={3}
          disabled={submitted}
        />
        <Answer
          content="Huawei (e.g. P20 Pro, 20 Mate)"
          selected={selectedAnswers.includes(4)}
          onClickAnswer={this.onClickAnswer}
          answerId={4}
          disabled={submitted}
        />
        <Answer
          content="None of the above"
          selected={selectedAnswers.includes(5)}
          onClickAnswer={this.onClickAnswer}
          answerId={5}
          disabled={submitted}
        />
        <Terms />
        <button className="submit-btn" onClick={this.onSubmitVote}>Submit Vote</button>
      </div>
    )
  }
}

export default QuestionContainer;