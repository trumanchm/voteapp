import React from 'react';
import { Router, Route } from 'react-router-dom';
import QuestionContainer from './QuestionContainer';
import Result from './Result';
import history from '../history';

class App extends React.Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <div>
            <Route path="/" exact component={QuestionContainer} />
            <Route path="/results" exact component={Result} />
          </div>
        </Router>
        <p className="advertisement">Advertisement</p>
      </div>
    )
  }
}

export default App;