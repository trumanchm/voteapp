import React from 'react';

const Terms = () => {
  return (
    <p className="terms">
      By submitting your vote, you consent to participate in the poll and consent to the use of your answers by Google for the sole purpose of their brand study. Click <a className="privacy-link" href="https://hypebeast.com/privacy">here</a> to learn about our Privacy Policy.
    </p>
  )
}
export default Terms;