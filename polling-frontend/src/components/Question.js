import React from 'react';

const Question = ({ label }) => {
  return (
    <div>
      <p><strong>This is a Google sponsored poll.</strong> Your answers will be part of a Google brand study on smartphone consumers.</p>
      <h3>{label}</h3>
    </div>
  )
}

export default Question;