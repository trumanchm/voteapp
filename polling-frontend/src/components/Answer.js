import React from 'react';

const getPercentage = (response, sum) => {
  return sum ? Math.round((response / sum) * 100) : 0;
}

const Answer = ({ selected, content, onClickAnswer, answerId, disabled, response, sum }) => {

  const percentage = getPercentage(response, sum);

  return (
    <div className="answer-wrapper" onClick={() => disabled ? {} : onClickAnswer(answerId)}>
      <span className={`answer ${selected ? 'selected-answer' : ''}`}>{content}</span>
      {disabled ?
        <div className="response-wrapper">
          <div className="response-bar" style={{ width: `${percentage}%` }} />
          <div className={`response-percentage ${selected ? 'selected' : ''}`}>{percentage}%</div>
        </div>
        : null}
    </div>
  )
}

export default Answer;