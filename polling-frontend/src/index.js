import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { iframeResizer } from 'iframe-resizer';

const iframe = document.querySelector('iframe');

iframe.setAttribute('id', 'voteFrame'); // add id for iframeResizer library for resize the iframe
iframe.removeAttribute('width');
iframe.removeAttribute('height');

// Resize the iframe size
iframeResizer({
  log: false,
  heightCalculationMethod: "max",
  minWidth: document.body.clientWidth

}, '#voteFrame')

// Keep attempt to render App component, will stop after rendered
var timer = setInterval(function () {
  const frameDocument = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document;

  if (frameDocument) {
    const targetContainer = frameDocument.querySelector('#root');

    if (targetContainer && targetContainer.innerHTML === "") {
      ReactDOM.render(
        <App />,
        targetContainer
      );

      clearInterval(timer);
    }
  }
}, 500);

